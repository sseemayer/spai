# spai
Simple publishing of accessible information

spai monitors one or more user-defined folders and sends all observed changes to a user-defined HTTP(s) endpoint. It is available as a statically-compiled command line application that should work without installing anything else onto the system.

## Download
Statically built binaries are automatically compiled. You can get the latest version for your operating system:

 * [x86_64-unknown-linux-musl](https://gitlab.com/sseemayer/spai/-/jobs/artifacts/master/raw/target/x86_64-unknown-linux-musl/release/spai?job=x86_64-unknown-linux-musl)
 * [x86_64-pc-windows-gnu](https://gitlab.com/sseemayer/spai/-/jobs/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/spai.exe?job=x86_64-pc-windows-gnu)


## Usage

```bash
$ spai -h
spai 0.1.0
Stefan Seemayer <mail+spai@semicolonsoftware.de>
Simple publishing of available information - monitors a folder and uploads changes to an HTTP API

USAGE:
    spai [OPTIONS] <PATH>... <URL>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --debounce <debounce>    Debounce interval in seconds

ARGS:
    <PATH>...    Provide a folder to monitor
    <URL>        API endpoint to notify
```

### More status information
You can set the environment variable `RUST_LOG` to the desired log level (`debug < info < warn < error`):

```bash
$ RUST_LOG=info spai myfolder http://api.mydomain.tld/watcher
```

If you only want to receive logs for the main `spai` application, you can specify it, too:

```bash
$ RUST_LOG=spai=info spai myfolder http://api.mydomain.tld/watcher
```

